import React from "react";
import Image from "next/image";
import Typed from "react-typed";
import { motion } from "framer-motion";
import { PTag } from "../atoms/typography";

const About = () => {
  return (
    <div className="flex flex-row gap-5 justify-center items-center m-auto p-10 overflow-hidden my-5">
      <div className="max-w-xl flex gap-3 flex-col">
        <PTag
          text="I am an enthusiastic person who wants to collaborate with an
      extraordinary team to learn more and upgrade my skills. I have been
      developing professionally for more than five years, but everything
      started in my school days with passion, self-learning, blogging, and
      freelancing."
        />
        <PTag
          text="Over the past five years, as a full-stack developer, I have developed
      and launched customized, highly responsive websites based on auction,
      finance, decentralized, education, e-commerce, and mass media concepts
      using advanced technologies in the market. In addition, I have
      consistently met client expectations and project milestones while
      working effectively in deadline-driven environments."
        />
        <PTag
          text="I also care about the people who will work after me on the project. If
      another developer works instead of me, he will see clean and readable
      code. One of my main goals is to write code that other developers can
      understand and work with effortlessly. Honesty and sincerity are
      crucial for me in work."
        />
        <PTag
          text="I would welcome any chance to provide further insight into my
      technical abilities and personal attributes in the future."
        />
      </div>
      <motion.div
        className="md:flex justify-center max-w-xs md:max-w-lg items-center flex-col md:p-10"
        animate={{ x: [100, 0] }}
        transition={{ type: "spring", duration: 2 }}
      >
        <Image
          src={"/illustrations/selecting.svg"}
          width={300}
          height={300}
          alt="selecting"
        />
      </motion.div>
    </div>
  );
};

export default About;
