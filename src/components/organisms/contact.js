import React from "react";
import { useEffect } from "react";
import Image from "next/image";
import { useInView } from "react-intersection-observer";
import { motion, useAnimation } from "framer-motion";

const variants = {
  hidden: (i) => ({ opacity: 0, y: 100 }),
  visible: (i) => ({
    opacity: [0, 1],
    y: [100, 100, 0, 0],
    transition: {
      delay: (i + 1) * 0.3,
      duration: 1,
    },
  }),
};

const Contact = () => {
  const controls = useAnimation();
  const [ref, inView] = useInView();

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    } else {
      controls.start("hidden");
    }
  }, [controls, inView]);
  return (
    <div className="flex md:flex-row flex-col justify-center items-center m-auto gap-5 p-5 pt-14 overflow-hidden">
      <div className="w-full flex flex-col gap-3 max-w-xs md:max-w-lg">
        <motion.div
          className="flex justify-between items-center gap-3"
          custom={0}
          animate={controls}
          initial={"hidden"}
          variants={variants}
        >
          <div ref={ref}>
            <Image src="/svg/mail.svg" alt="me" width="30" height="30" />
          </div>
          <span className="text-lg font-mono text-black-300">
            itsjakirhussain@gmail.com
          </span>
        </motion.div>
        <motion.div
          className="flex justify-between items-center gap-3"
          custom={1}
          animate={controls}
          initial={"hidden"}
          variants={variants}
        >
          <div>
            <Image src="/svg/phone.svg" alt="me" width="30" height="30" />
          </div>
          <span className="text-lg font-mono text-black-300">
            +91 9629180060
          </span>
        </motion.div>
      </div>
      <motion.div
        className="md:flex justify-center max-w-xs md:max-w-lg items-center flex-col md:p-10"
        custom={2}
        animate={controls}
        initial={"hidden"}
        variants={variants}
      >
        <Image
          src={"/illustrations/personal_text.svg"}
          width={400}
          height={400}
          alt="selecting"
        />
      </motion.div>
    </div>
  );
};

export default Contact;
