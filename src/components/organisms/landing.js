import React from "react";
import { motion } from "framer-motion";

import Intro from "../molecules/intro";
import Actions from "../molecules/actions";
import Programmer from "../molecules/programmer";
import ScoreBoard from "../molecules/score-board";
import SocialIcons from "../molecules/social-icons";

const Landing = () => {
  return (
    <motion.div
      className="flex flex-col md:justify-center justify-start items-center w-auto h-full flex-grow p-0 sm:p-5"
      animate={{ rotate: [2, -2, 0] }}
      transition={{ type: "spring", duration: 2 }}
    >
      <motion.div className="flex cursor-default w-full md:justify-around justify-center items-center flex-col md:flex-row md:p-2 pt-10 flex-wrap">
        <Intro />
        <Programmer />
      </motion.div>
      <motion.div className="bg-gray md:bg-grey-100 w-full flex justify-center items-center flex-col rounded-lg md:p-5 mb-5">
        <Actions className="pb-5 md:hidden" />
        <ScoreBoard />
        <SocialIcons />
      </motion.div>
    </motion.div>
  );
};

export default Landing;
