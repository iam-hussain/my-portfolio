import React from "react";
import Typed from "react-typed";
import { motion } from "framer-motion";
import { roleData } from "../../constant";
import Paragraph, { Name, H4 } from "../../components/atoms/typography";

const Intro = () => {
  return (
    <motion.div
      className="md:min-w-4/12 max-w-3xl flex justify-center align-middle items-center flex-col"
      animate={{ x: [-200, 0] }}
      transition={{ type: "spring", duration: 1 }}
    >
      <H4 text="Hi there! I am" />
      <Name />
      <Typed
        className="md:text-xl text-center uppercase font-sans font-semibold text-base"
        strings={roleData}
        typeSpeed={40}
        backSpeed={50}
        loop
      />
      <Paragraph
        text="I code and develop beautiful things, and I love what I do"
        className="pt-2 w-8/12 md:w-full"
      />
    </motion.div>
  );
};

export default Intro;
