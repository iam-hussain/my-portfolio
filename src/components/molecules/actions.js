import React from "react";
import { motion } from "framer-motion";
import classNames from "classnames";
import { LinkButton } from "../atoms/button";
import { docsData } from "../../constant";

const Actions = ({ className = "" }) => {
  return (
    <motion.div
      className={classNames(
        "md:gap-10 gap-4 flex",
        { [className]: className !== "" }
      )}
      animate={{ x: [500, 0] }}
      transition={{ type: "spring", duration: 1 }}
    >
      <LinkButton text="View CV" newTab href={docsData.download} download />
      <LinkButton
        text="Hire Me"
        newTab
        href={docsData.hire_me}
        className="bg-black-600 text-white"
      />
    </motion.div>
  );
};

export default Actions;
