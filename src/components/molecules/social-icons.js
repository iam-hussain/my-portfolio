import React from "react";
import { useEffect } from "react";
import { useInView } from "react-intersection-observer";
import {  useAnimation } from "framer-motion";
import { socialData } from "../../constant";
import { LinkIconButton } from "../atoms/button";

const variants = {
  hidden: (i) => ({ opacity: 0, x: i * 100 }),
  visible: (i) => ({
    opacity: [0, 1],
    x: [i * 100, 0],
    transition: {
      delay: (i + 1) * 0.3,
    },
  }),
};

const SocialIcons = () => {
  const controls = useAnimation();
  const [ref, inView] = useInView();

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    } else {
      controls.start("hidden");
    }
  }, [controls, inView]);
  return (
    <div
      className="flex md:justify-between justify-center items-center md:gap-5 gap-0 max-w-3xl flex-wrap md:pt-5 md:px-5 w-full rounded-sm overflow-hidden"
      ref={ref}
    >
      {socialData.map((e, i) => (
        <LinkIconButton
          key={i}
          {...e}
          newTab
          custom={i}
          animate={controls}
          initial={"hidden"}
          variants={variants}
        />
      ))}
    </div>
  );
};

export default SocialIcons;
