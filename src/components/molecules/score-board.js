import React from "react";
import { motion } from "framer-motion";
import Counter from "../atoms/counter";
import Paragraph from "../atoms/typography";

import { boardData } from "../../constant";

const ScoreBoard = () => {
  return (
    <motion.div className="flex md:justify-between items-center md:gap-10 gap-3 pb-5 cursor-default max-w-3xl justify-center">
      {boardData.map((e, i) => (
        <motion.div
          key={i}
          className="flex flex-col justify-center items-center w-auto"
          animate={{ scale: [0, 1], opacity: [0, 1] }}
          transition={{ type: "spring", duration: 0.5 }}
        >
          <Counter
            from={1}
            to={e.value}
            className="text-green md:text-4xl font-sans font-medium text-2xl"
            duration={e.duration}
            suffix="+"
          />
          <Paragraph
            className="uppercase pt-1"
            animate={{ opacity: [0, 1] }}
            transition={{ type: "spring", duration: 2 }}
            text={e.title}
          />
        </motion.div>
      ))}
    </motion.div>
  );
};

export default ScoreBoard;
