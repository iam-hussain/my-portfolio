import React from "react";
import { motion } from "framer-motion";
import classNames from "classnames";

const Paragraph = ({ text, className, ...args }) => {
  return (
    <motion.p
      className={classNames(
        "text-black-200 font-sans text-sm md:text-base text-center",
        { [className]: className !== "" }
      )}
      animate={{ opacity: [0, 1] }}
      transition={{ type: "spring", duration: 2 }}
      {...args}
    >
      {text}
    </motion.p>
  );
};

export const PTag = ({ text, className, ...args }) => {
  return (
    <motion.p
      className={classNames(
        "text-black-600 font-mono text-lg text-right font-serif",
        { [className]: className !== "" }
      )}
      animate={{ opacity: [0, 1] }}
      transition={{ type: "spring", duration: 2 }}
      {...args}
    >
      {text}
    </motion.p>
  );
};

export const Name = () => {
  return (
    <motion.div className="md:text-6xl text-4xl uppercase py-2 font-bold w-full flex justify-center gap-3 select-none">
      <motion.span
        animate={{ x: [0, 0, 10, 10, 0] }}
        transition={{ type: "spring", repeat: Infinity, duration: 2 }}
      >
        Jakir
      </motion.span>
      <motion.span
        className="text-green"
        animate={{ x: [0, 0, -10, -10, 0] }}
        transition={{ type: "spring", repeat: Infinity, duration: 2 }}
      >
        Hussain
      </motion.span>
    </motion.div>
  );
};

export const H4 = ({ text, className, ...args }) => {
  return (
    <motion.h4
      className={classNames(
        "md:text-3xl text-xl font-sans font-semibold text-black-300",
        { [className]: className !== "" }
      )}
      {...args}
    >
      {" "}
      {text}
    </motion.h4>
  );
};

export const H6 = ({ text, className, ...args }) => {
  return (
    <motion.h6
      className={classNames(
        "font-mono text-black-300 uppercase font-semibold text-lg",
        { [className]: className !== "" }
      )}
      {...args}
    >
      {text}
    </motion.h6>
  );
};

export default Paragraph;
