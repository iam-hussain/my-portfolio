import React from "react";
import Link from "next/link";
import Image from "next/image";
import { motion } from "framer-motion";
import classNames from "classnames";

const Button = ({ text, onClick, className = "" }) => {
  const handleOnClick = (e) => {
    if (onClick) {
      onClick(e);
    }
  };
  return (
    <motion.button
      className={classNames(
        "font-mono uppercase font-semibold px-4 py-2 rounded-sm cursor-pointer",
        { "bg-green-300": className === "" },
        { "text-black-600": className === "" },
        { [className]: className !== "" }
      )}
      whileHover={{ scale: 1.2 }}
      whileTap={{ scale: 0.8 }}
      onClick={handleOnClick}
    >
      {text}
    </motion.button>
  );
};

export const LinedButton = ({ text, onClick, className = "" }) => {
  const handleOnClick = (e) => {
    if (onClick) {
      onClick(e);
    }
  };
  return (
    <motion.button
      className={classNames(
        "border py-2 px-4 text-sm font-semibold rounded-sm cursor-pointer",
        { "border-green-300": className === "" },
        { "text-black-600": className === "" },
        { [className]: className !== "" }
      )}
      whileHover={{ scale: 1.2, color: "#3f3d56" }}
      whileTap={{ scale: 0.8 }}
      onClick={handleOnClick}
    >
      {text}
    </motion.button>
  );
};

export const AnchorButton = ({ text, className = "", newTab, ...args }) => {
  return (
    <motion.a
      className={classNames(
        "font-mono uppercase font-semibold px-4 py-2 rounded-sm cursor-pointer",
        { "bg-green-300": className === "" },
        { "text-black-600": className === "" },
        { [className]: className !== "" }
      )}
      whileHover={{ scale: 1.2 }}
      whileTap={{ scale: 0.8 }}
      target={newTab ? "_blank" : "_self"}
      {...args}
    >
      {text}
    </motion.a>
  );
};

export const LinkButton = ({
  href,
  className,
  newTab = false,
  text,
  ...arg
}) => {
  return (
    <Link href={href} passHref={true} {...arg}>
      <AnchorButton text={text} className={className} newTab={newTab} />
    </Link>
  );
};

export const AnchorIconButton = ({
  className = "",
  src,
  alt,
  height,
  width,
  newTab,
  ...args
}) => {
  return (
    <motion.a
      className={classNames(
        "md:h-20 md:w-20 h-12 w-12 flex justify-center items-center rounded-sm cursor-pointer",
        { "bg-white": className === "" },
        { "text-black-600": className === "" },
        { [className]: className !== "" }
      )}
      whileHover={{
        scale: [1, 1.2, 1.2, 1, 1],
        rotate: [0, 0, 270, 270, 0],
        borderRadius: [0, 0, 50, 50, 0],
      }}
      whileTap={{ scale: 0.8 }}
      target={newTab ? "_blank" : "_self"}
      {...args}
    >
      <motion.div className="flex justify-center items-center">
        <Image src={src} alt={alt} height={height} width={width} />
      </motion.div>
    </motion.a>
  );
};

export const LinkIconButton = ({
  href,
  className,
  newTab = false,
  src,
  alt,
  height,
  width,
  ...arg
}) => {
  return (
    <Link href={href} passHref={true}>
      <AnchorIconButton
        className={className}
        src={src}
        alt={alt}
        height={height}
        width={width}
        newTab={newTab}
        {...arg}
      />
    </Link>
  );
};

export default Button;
