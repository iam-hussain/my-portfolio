import React from "react";
import NavBar from "../components/organisms/nav-bar";
import Landing from "../components/organisms/landing";
import Skills from "../components/organisms/skills";
import Contact from "../components/organisms/contact";

const Home = () => {
  return (
    <div className="mb-14 text-black mx-auto select-none">
      <div className="container mx-auto min-h-screen flex justify-start flex-col">
        <NavBar />
        <Landing />
      </div>
      <Skills />
      <Contact />
    </div>
  );
};

export default Home;
