export const roleData = [
  "Software Engineer",
  "Javascript expert",
  "Front-end Developer",
  "Back-end Developer",
  "Node JS Developer",
  "React JS Developer",
];

export const boardData = [
  {
    title: "Years of Experience",
    value: 5,
    duration: 1,
  },{
    title: "Projects worked",
    value: 20,
    duration: 2,
  },{
    title: "Known Technology",
    value: 50,
    duration: 3,
  }
]

export const docsData = {
  download: "https://file.jhussain.com/resume/jakir_hussain_resume_compressed.pdf",
  hire_me: "https://www.upwork.com/freelancers/~01962ee1e2e11317a4"
}
export const socialData = [
  {
    src: "/svg/github.svg",
    height: 25,
    width: 25,
    alt: "GitHub",
    href: "https://github.com/iam-hussain"
  },
  {
    src: "/svg/gitlab.svg",
    height: 25,
    width: 25,
    alt: "GitLab",
    href: "https://gitlab.com/iam-hussain"
  },
  {
    src: "/svg/upwork.svg",
    height: 25,
    width: 50,
    alt: "UpWorks",
    href: "https://www.upwork.com/freelancers/~01962ee1e2e11317a4"
  },
  {
    src: "/svg/linkedin.svg",
    height: 25,
    width: 25,
    alt: "LinkedIn",
    href: "https://www.linkedin.com/in/iam-hussain"
  },
  {
    src: "/svg/npm.svg",
    height: 25,
    width: 35,
    alt: "NPM",
    href: "https://www.npmjs.com/~iam-hussain"
  },
  {
    src: "/svg/stackoverflow.svg",
    height: 25,
    width: 25,
    alt: "StackOverFlow",
    href: "https://stackoverflow.com/users/13712263/hussain"
  },
  {
    src: "/svg/twitter.svg",
    height: 25,
    width: 25,
    alt: "Twitter",
    href: "https://twitter.com/_i_am_hussain"
  },
]

export const skillsData = [
  {
    title: "TOP SKILLS",
    items: [
      "React JS / Next JS",
      "Node JS / Express JS",
      "MongoDB",
      "MySQL",
      "GraphQL",
      "Styled Component",
      "Web3 JS / Ethers JS",
      "Solidity / ERC Tokens",
      "Truffle / Hardhat",
    ],
  },

  {
    title: "Framework / Libraries",
    items: [
      "Redux",
      "Bootstrap / Tailwind",
      "Material UI",
      "Apollo GraphQL",
      "Sequelize ORM",
      "Mongoose ODM",
      "Jest / Mocha / Chai",
      "Storybook",
      "Cypress.IO",
      "Webpack / Babel",
      "Elastic Search",
      "Socket.IO",
      "HandleBar JS / EJS",
      "Framer Motion"
    ],
  },
  {
    title: "DATABASE",
    items: ["MySQL", "PostgreSQL", "MongoDB", "DynamoDB", "Redis"],
  },
  {
    title: "LANGUAGES",
    items: [
      "JavaScript",
      "TypeScript",
      "GraphQL",
      "HTML",
      "CSS",
      "SCSS / LESS",
      "Solidity",
      "SQL",
    ],
  },
  {
    title: "Tools / Others",
    items: ["Nginx", "Docker", "AWS", "Serverless Computing", "NPM / YARN", "NVM", "GIT"],
  },

  // {
  //   title: "learning",
  //   items: ["React Native", "Flutter / Dart"],
  // },
];
