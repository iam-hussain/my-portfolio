module.exports = {
  content: [
    "./src/pages/*.js",
    "./src/pages/**/*.js",
    "./src/components/*.js",
    "./src/components/**/*.js",
    "./src/components/**/**/*.js",
  ],
  theme: {
    colors: {
      'green': '#00BFA6',
      'green-500': '#00bfa6bf',
      'green-400': '#00bfa6b3',
      'green-300': '#00bfa6a8',
      'green-100': '#00bfa64d',
      'white': '#FFF',
      'black': '#262534',
      'black-600': '#2f2d41',
      'black-300': '#3f3d56',
      'black-200': '#3f3d56bd',
      'black-100': '#3f3d5659',
      "grey-100": '#efefef'
    },
  },
  plugins: [],
}