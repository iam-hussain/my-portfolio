module.exports = {
  handleImages: ['jpeg', 'png', 'svg'],
  enableSvg: true,
  images: {
    loader: 'imgix',
    path: 'https://file.jhussain.com/jhussain',
  },
  webpack(config) {
    return config;
  },
};
